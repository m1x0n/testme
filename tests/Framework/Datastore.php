<?php namespace Dres\Tests;

interface Datastore
{
    public function setup();

    public function teardown();
}