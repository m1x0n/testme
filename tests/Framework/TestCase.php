<?php namespace Dres\Tests;

use Dres\Tests\DatastoreStrategy\MySQL\InMemoryTransactional;

class TestCase extends \Laravel\Lumen\Testing\TestCase
{
    protected static $cached_app;

    public function setUp()
    {
        parent::setUp();
        $this->dataStoreStrategy()->setUp();
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        if (!static::$cached_app) {
            static::$cached_app = require __DIR__ . '/../../../../../bootstrap/app.php';
        }
        return static::$cached_app;
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown()
    {
        if (class_exists('Mockery')) {
            \Mockery::close();
        }

        $this->dataStoreStrategy()->tearDown();
    }

    protected function dataStoreStrategy()
    {
        return new InMemoryTransactional($this);
    }

}
