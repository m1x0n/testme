<?php

namespace Tests\Acceptance;

use App\Service\Mailing\Contracts\Sender;
use TestCase;
use Tests\MailTracking;

abstract class AcceptanceTest extends TestCase
{
    use MailTracking;

    /**
     * @var Sender
     */
    protected $sender;

    public function setUp()
    {
        parent::setUp();

        $this->sender = app(Sender::class);

        $this->setUpMailTracking();
    }
}
