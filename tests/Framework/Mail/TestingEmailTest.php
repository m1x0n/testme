<?php

use App\Service\Mailing\Emails\TestEmail;
use Tests\Acceptance\AcceptanceTest;

class TestingEmailTest extends AcceptanceTest
{
    public function test_should_send_test_email()
    {
        $this->sender->send(new TestEmail('John Doe'));

        $this->seeEmailWasSent();
        $this->seeEmailContains('John Doe');
    }
}
