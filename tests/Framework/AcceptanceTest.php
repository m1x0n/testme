<?php namespace Dres\Tests;

use Dres\Tests\Helpers\FixtureRequest;
use Dres\Tests\Helpers\WithEventQueue;
use Dres\Tests\Helpers\AcceptanceRequest;
use Dres\Tests\Acceptance\Security;
use Dres\Tests\Acceptance\EventSeeder;

abstract class AcceptanceTest extends TestCase
{
    use FixtureRequest, WithEventQueue, AcceptanceRequest, Security;

    const TOKEN_THAT_EXPIRES_IN_3000AD = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0Mjc1YzRlYS0wODgxLTQzYjctOTFlNC00N2MwMDY1NmUwMjAiLCJpc3MiOiJkeW5hbWljcmVzIiwiYXVkIjoiRHluYW1pY1JlcyBBUEkiLCJ0eXBlIjoiYWdlbnQiLCJ1c2VyIjp7ImlkIjoiYTIxOWY0YjctNzE3Yi00NTFjLWE0ZGMtMDZkODZiMzY5NThhIiwicm9sZXMiOlsiYWdlbnQiXSwiY2xhaW1zIjpbeyJsYWJlbCI6ImFnZW5jeSIsInJlc291cmNlX2lkIjoiNWYzYWM0YjMtYTZhMi00OTU5LTk3OWYtYTk2Yzc4OGE4OWJlIn0seyJsYWJlbCI6ImJyYW5kIiwicmVzb3VyY2VfaWQiOiJhbGwifV19LCJpYXQiOjE0NzUxNTc1MjUsImV4cCI6MzI1MDM2NzI4MDAsInZlciI6MX0.YIIQ9skxn96QgylzMH1mMgunQmnlwdx8nrQb1p5041I";
    const ADMIN_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0Mjc1YzRlYS0wODgxLTQzYjctOTFlNC00N2MwMDY1NmUwNTUiLCJpc3MiOiJkeW5hbWljcmVzIiwiYXVkIjoiRHluYW1pY1JlcyBBUEkiLCJ0eXBlIjoic3VwZXJfYWRtaW4iLCJ1c2VyIjp7ImlkIjoiYTIxOWY0YjctNzE3Yi00NTFjLWE0ZGMtMDZkODZiMzY5NWJiIiwicm9sZXMiOlsic3VwZXItYWRtaW4iXSwiY2xhaW1zIjpbeyJsYWJlbCI6InJvbGUiLCJyZXNvdXJjZV9pZCI6InN1cGVyX2FkbWluIn1dfSwiaWF0IjoxNDc1MTU3NTI1LCJleHAiOjMyNTAzNjcyODAwLCJ2ZXIiOjF9.heMPFQaGZ1hlV7aphFzO4VmvUpYcVOaMwyNc_tHBYKE";
    const DISPATCH_ENDPOINT = '/dispatch';

    public function setUp()
    {
        parent::setUp();
        $this->setUpEventQueue();
        $this->disableHttpSecurity();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->enableHttpSecurity();
    }

    protected function processEvent($fixture_name)
    {
        $event_fixture = $this->createRequest($fixture_name);

        EventSeeder::prepareEvent($event_fixture);

        $this->artisan('db:seed', ['--class' => EventSeeder::class]);

        $this->artisan("workflows:play");
    }

    /**
     * @return string
     */
    abstract protected function fixtureFolder();
}
