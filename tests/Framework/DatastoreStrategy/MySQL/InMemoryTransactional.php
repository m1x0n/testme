<?php namespace Dres\Tests\DatastoreStrategy\MySQL;

use Dres\Tests\Datastore;
use Laravel\Lumen\Testing\TestCase;

class InMemoryTransactional implements Datastore
{
    private $testcase;
    private $db;
    private $clear_tables;

    public function __construct(TestCase $testcase)
    {
        $this->db = app('db');
        $this->clear_tables = new ClearTables($this->db);
        $this->testcase = $testcase;
    }

    private static $is_first_run = true;

    public function setUp()
    {
        if (self::$is_first_run) {
            $this->clear_tables->clear();
            $this->testcase->artisan('migrate');
            self::$is_first_run = false;
        }
        $this->db->beginTransaction();
    }

    public function tearDown()
    {
        $this->db->rollback();
    }
}