<?php namespace Dres\Tests\DatastoreStrategy\MySQL;

class ClearTables
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function clear()
    {
        $tables = array_map('reset', $this->db->select('SHOW TABLES'));

        foreach ($tables as $table) {
            $this->db->raw("DELETE TABLE $table");
        }
    }
}