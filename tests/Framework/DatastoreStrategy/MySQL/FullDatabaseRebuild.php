<?php namespace Dres\Tests\DatastoreStrategy\MySQL;

use Dres\Tests\Datastore;
use Laravel\Lumen\Testing\TestCase;

class FullDatabaseRebuild implements Datastore
{
    private $db;
    private $testcase;
    private $clear_tables;

    public function  __construct(TestCase $testcase)
    {
        $this->db = app('db');
        $this->testcase = $testcase;
        $this->clear_tables = new ClearTables($this->db);
    }

    public function setup()
    {
        $this->clear_tables->clear();
        $this->testcase->artisan('migrate');
    }

    public function teardown()
    {

    }
}