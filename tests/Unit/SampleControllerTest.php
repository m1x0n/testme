<?php

namespace Tests\Unit;

use App\Http\Controllers\SampleController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class SampleControllerTest extends TestCase
{
    public function test_should_echo_input_data_on_store()
    {
        $controller = new SampleController();

        $echoData = ['foo' => 'bar'];

        $request = Request::create(
            '/sample',
            'POST',
            [],
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($echoData)
        );
        $response = $controller->store($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertEquals($echoData, $response->getData(true));
    }
}
