<?php

namespace Infrastructure;

use App\Contracts\User;
use App\Contracts\UserRepository;

class InMemoryUserRepository implements UserRepository
{
    private $users;

    public function __construct()
    {
        $this->users = [
            '1' => new \App\Models\User('1', 'John'),
            '5' => new \App\Models\User('5', 'John'),
            '10' => new \App\Models\User('10', 'John'),
        ];
    }

    public function byId(string $userId): ?User
    {
        return $this->users[$userId] ?? null;
    }
}
