<?php

namespace Infrastructure;

use App\Contracts\User;
use App\Contracts\UserRepository;
use GuzzleHttp\Client;

class RemoteUserRepository implements UserRepository
{
    private $httpClient;

    public function __construct($httpClient = null)
    {
        $this->httpClient = $httpClient ?: new Client();
    }

    public function byId(string $userId): ?User
    {
        try {
            //TODO: show mock builder examples
            $response = $this->httpClient->get("https://randomuser.me/api/?inc=name,gender,email");
            $content = json_decode($response->getBody()->getContents());

            if (empty($content)) {
                return null;
            }

            return new \App\Models\User($userId, $content['name']);
        } catch (\Throwable $exception) {
            return null;
        }
    }
}
