# Test me

1. Unit tests
    1.1 Simple Controller/Handler unit tests w\o dependencies
    1.2 Simple Controller/Handler unit test with mocked dependencies
    1.3 Simple middleware test w\o dependencies
    1.4 Simple middleware test with mocked dependencies
    1.5 Service tests
    1.6 Mocks, stubs, fakes, anonymous classes
    1.7 Custom test cases

2. API tests (db, sockets, third-party)
    - In memory database (wrap everything in transaction during test run)
    - Fixtures (files, jsons)
    - Mail sent test aka event interceptor
