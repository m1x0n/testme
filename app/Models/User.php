<?php

namespace App\Models;

use App\Contracts\User as UserContract;

class User implements UserContract
{
    private $id;
    private $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }

    public function fromArray(array $data): User
    {
        return new self($data['id'], $data['name']);
    }
}
