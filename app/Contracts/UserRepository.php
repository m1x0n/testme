<?php

namespace App\Contracts;

interface UserRepository
{
    public function byId(string $userId): ?User;
}
