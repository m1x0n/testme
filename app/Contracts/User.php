<?php

namespace App\Contracts;

interface User
{
    public function id(): string;

    public function name(): string;
}
