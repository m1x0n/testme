<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SampleController
{
    public function store(Request $request)
    {
        $data = $request->toArray();

        return response()->json($data);
    }
}
